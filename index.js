const express = require('express')
const path = require('path')
const app = express()

require('dotenv-flow').config()
const config = require('./config/index.js')

const Iyzipay = require('iyzipay')
const iyzipay = new Iyzipay(config)

const exphbs = require('express-handlebars')
app.engine('handlebars', exphbs())
app.set('view engine', 'handlebars')

app.use(express.static(path.join(__dirname, 'public')))

app.get('/', function (req, res) {

    console.log("in main page")

    delete req.app.locals.requestOrder, req.app.locals.successOrder
    res.render('home', {pageInfo: 'Main Page'})
})

app.get('/paymentform', function (req, res) {

    console.log("in paymentform")

    var request = {
        locale: Iyzipay.LOCALE.TR,
        conversationId: '123456789',
        price: '15',
        paidPrice: '15',
        currency: Iyzipay.CURRENCY.TRY,
        basketId: 'B67832',
        paymentGroup: Iyzipay.PAYMENT_GROUP.PRODUCT,
        callbackUrl: 'http://localhost:3000/callback',
        enabledInstallments: [2, 3, 6, 9],
        buyer: {
            id: 'BY789',
            name: 'John',
            surname: 'Doe',
            gsmNumber: '+905350000000',
            email: 'email@email.com',
            identityNumber: '74300864791',
            lastLoginDate: '2015-10-05 12:43:35',
            registrationDate: '2013-04-21 15:12:09',
            registrationAddress: 'Nidakule Göztepe, Merdivenköy Mah. Bora Sok. No:1',
            ip: '85.34.78.112',
            city: 'Istanbul',
            country: 'Turkey',
            zipCode: '34732'
        },
        shippingAddress: {
            contactName: 'Jane Doe',
            city: 'Istanbul',
            country: 'Turkey',
            address: 'Nidakule Göztepe, Merdivenköy Mah. Bora Sok. No:1',
            zipCode: '34742'
        },
        billingAddress: {
            contactName: 'Jane Doe',
            city: 'Istanbul',
            country: 'Turkey',
            address: 'Nidakule Göztepe, Merdivenköy Mah. Bora Sok. No:1',
            zipCode: '34742'
        },
        basketItems: [
            {
                id: 'BI101',
                name: 'Binocular',
                category1: 'Collectibles',
                category2: 'Accessories',
                itemType: Iyzipay.BASKET_ITEM_TYPE.PHYSICAL,
                price: '5'
            },
            {
                id: 'BI102',
                name: 'Game code',
                category1: 'Game',
                category2: 'Online Game Items',
                itemType: Iyzipay.BASKET_ITEM_TYPE.VIRTUAL,
                price: '5'
            },
            {
                id: 'BI103',
                name: 'Usb',
                category1: 'Electronics',
                category2: 'Usb / Cable',
                itemType: Iyzipay.BASKET_ITEM_TYPE.PHYSICAL,
                price: '5'
            }
        ]
    };

    iyzipay.checkoutFormInitialize.create(request, function (err, result) {
        if (result.status !== 'success') {
            console.log('A payment error occurred: ' + result.status + ' - ' + result.errorMessage)
            return res.json({
                status: result.status,
                message: result.errorMessage
            });
        }
        req.app.locals.requestOrder = result
        res.json(result.checkoutFormContent)
    });
})

app.post('/callback', function (req, res) {

    console.log("in callback")

    iyzipay.checkoutForm.retrieve({
        locale: Iyzipay.LOCALE.TR,
        conversationId: req.app.locals.requestOrder.conversationId,
        token: req.app.locals.requestOrder.token
    }, function (err, result) {
        if (result.status !== 'success' && result.paymentStatus !== "SUCCESS") {
            console.log('A payment callback error occurred: ' + result.status + ' - ' + result.errorMessage)
            return res.json({
                status: result.status,
                message: result.errorMessage
            });
        }
        req.app.locals.successOrder = result
        res.render('home', {pageInfo: 'Succes Page', successOrder: req.app.locals.successOrder})
    });
})

app.get('/refund', function (req, res) {

    console.log("in refund")

    try {

        var singleRefund = function (data) {
            return new Promise(function (resolve, reject) {
                return iyzipay.refund.create({
                    locale: Iyzipay.LOCALE.TR,
                    conversationId: req.app.locals.successOrder.conversationId,
                    paymentTransactionId: data.paymentTransactionId,
                    price: data.paidPrice,
                    currency: Iyzipay.CURRENCY.TRY,
                    ip: '85.34.78.112'
                }, function (err, result) {
                    return resolve(result)
                })
            })
        }

        var promises = []
        var i = 0
        while (typeof req.app.locals.successOrder.itemTransactions[i] !== "undefined") {
            promises.push(singleRefund(req.app.locals.successOrder.itemTransactions[i]))
            i++
        }

        return Promise.all(promises).then((values) => {
            res.json({
                status: true,
                message: values
            })
        })

    } catch (e) {
        res.json({
            status: false,
            message: e.toString()
        })
    }

})

app.listen(3000, function () {
    console.log('Entropi .. iyzico basic: 3000');
})
