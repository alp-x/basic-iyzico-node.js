const config = {
    environment: process.env.NODE_ENV || 'production',
    uri: process.env.API_URI,
    apiKey: process.env.API_KEY,
    secretKey: process.env.SECRET_KEY
}

module.exports = config