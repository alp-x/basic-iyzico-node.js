$("#payment-form").click(function (data) {
    $.get("/paymentform", function (data, status) {
        if (data.status !== 'failure') {
            $('#iyzipay-checkout-form').html(data)
        } else if (data.status === 'failure') {
            $('#error-message').text(data.status + ' - ' + data.message).parent().removeClass('d-none')
        }
    })
})

$("#refund").click(function (data) {
    $.get("/refund", function (data, status) {

        var success_message_template_start = `<p class="alert alert-success" role="alert">`
        var failed_message_template_start = `<p class="alert alert-danger" role="alert">`
        var message_template_end = `</p>`

        if (data.status) {

            var total_html = {
                success: ``,
                failed: ``
            }

            data.message.map(function (x) {
                if (x.status !== "failure") {
                    total_html["success"] += `${success_message_template_start} ${x.status} - ${x.conversationId} - ${x.paymentTransactionId} ${message_template_end}`
                } else {
                    total_html["failed"] += `${failed_message_template_start} ${x.status} - ${x.conversationId} - ${x.paymentTransactionId} ${message_template_end}`
                }
            })

            $('.success-messages')
                .html(total_html["success"])
                .removeClass('d-none')

            $('.failed-messages')
                .html(total_html["failed"])
                .removeClass('d-none')


        } else {
            $('.error-messages').html(
                `${failed_message_template_start}
                status: ${data.status}
                ${message_template_end}
                ${failed_message_template_start}
                message: ${data.message}
                ${message_template_end}`
            ).removeClass('d-none')
        }

    })
})
